This readme provides detailed instructions for setting up and running the project using Python 3.8+, Conda, and various dependencies. Follow these steps to ensure a smooth installation process.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation Steps](#installation-steps)
  - [Python Setup](#python-setup)
  - [Conda Setup](#conda-setup)
  - [Creating Conda Environment](#creating-conda-environment)
  - [Activating Conda Environment](#activating-conda-environment)
  - [Installing Dependencies](#installing-dependencies)
- [Running the Project](#running-the-project)

---

## Prerequisites<a name="prerequisites"></a>

Before you proceed with the installation, make sure you have the following prerequisites in place:

- Python 3.8+ is installed on your system.
- You have administrative access to your computer.
- GPU enabled system

## Installation Steps<a name="installation-steps"></a>

Follow these steps to set up the project environment and install the necessary dependencies:

### Python Setup<a name="python-setup"></a>

1. Install Python 3.8 or a higher version if not already installed on your system.

2. Add the Python installation path to your system's environmental variables.

3. Restart your IDE (Visual Studio Code).

### Conda Setup<a name="conda-setup"></a>

1. Install Miniconda or Anaconda (Conda) for Python 3.8+ from the official website.

2. Add the Conda installation paths to your system's environmental variables, paths can be different from system to system:
   - `C:\ProgramData\miniconda3`
   - `C:\ProgramData\miniconda3\Scripts`

3. Restart Visual Studio Code.

### Creating Conda Environment<a name="creating-conda-environment"></a>

1. Open a terminal or PowerShell as an administrator.

2. Navigate to the Conda binary directory:
   ```
   cd ProgramData >> miniconda3 >> condabin
   ```

3. Run the following command to initialize Conda in PowerShell:
   ```
   ./conda init powershell
   ```

### Create and Activate Conda Environment<a name="activating-conda-environment"></a>

1. ```conda create --name <env_name> python=3.8```

2. Activate your Conda environment using PowerShell: 
#### cd ProgramData >> miniconda3 ####
   ```
   ./Scripts/activate conda_env
   ```

### Installing Dependencies<a name="installing-dependencies"></a>

1. Change your current working directory to your project directory.

2. Append necessary Conda channels:
   ```
   conda config --append channels conda-forge
   conda config --append channels Nvidia
   ```

3. Install CUDA toolkit:
   ```
   conda install cudatoolkit
   ```

4. Uninstall existing Torch (if any):
   ```
   pip uninstall torch
   pip cache purge
   ```

5. Install the latest version of Torch:
   ```
   pip install torch -f https://download.pytorch.org/whl/torch_stable.html
   ```

6. Install requirements from `requirements.txt`:
   ```
   pip install -r requirements.txt
   ```

7. Install specific version of `mmcv` using Tsinghua University mirror:
   ```
   mim install "mmcv>=2.0.0rc1" -i https://pypi.tuna.tsinghua.edu.cn/simple
   ```

8. Install additional required packages:
   ```
   mim install mmdet mmpose mmengine
   ```

## Running the Project<a name="running-the-project"></a>

With the environment set up and dependencies installed, you are ready to run the project.

```nvicorn main:app --reload```

---

Please note that these instructions assume a Windows environment and PowerShell as the shell. If you are using a different operating system or shell, some steps may vary. Make sure to consult the official documentation for any discrepancies or if you encounter any issues during the installation process.