import os
import cv2
import mmcv
import torch
import mmengine
import tempfile
import numpy as np
import torch
import cloudinary.uploader
import asyncio
# from google.cloud import storage

from mmaction.utils import frame_extract
from mmaction.registry import VISUALIZERS
from mmengine.utils import track_iter_progress
from mmaction.apis import (detection_inference, inference_recognizer,
                           init_recognizer, pose_inference)

from pathlib import Path
from fastapi import FastAPI, File, UploadFile

try:
    import moviepy.editor as mpy
except ImportError:
    raise ImportError('Please install moviepy to enable output file')

FONTFACE = cv2.FONT_HERSHEY_DUPLEX
FONTSCALE = 0.75
FONTCOLOR = (255, 255, 255)  # BGR, white
THICKNESS = 1
LINETYPE = 1
DEVICE = 'cuda'
DET_SCORE_THR = 0.9

checkpoint = 'best_acc_top1_epoch_24.pth'
label_map_txt = 'label_map_ntu60.txt'
det_config = 'faster-rcnn_r50_fpn_2x_coco_infer.py'
pose_config = 'td-hm_hrnet-w32_8xb64-210e_coco-256x192_infer.py'
action_config = 'slowonly_r50_8xb16-u48-240e_ntu60-xsub-keypoint.py'

### url link to the detection and pose models
pose_checkpoint = 'https://download.openmmlab.com/mmpose/top_down/hrnet/hrnet_w32_coco_256x192-c78dce93_20200708.pth'
det_checkpoint = 'http://download.openmmlab.com/mmdetection/v2.0/faster_rcnn/faster_rcnn_r50_fpn_2x_coco/faster_rcnn_r50_fpn_2x_coco_bbox_mAP-0.384_20200504_210434-a5d8aa15.pth'

app = FastAPI()
cloudinary.config( 
  cloud_name = "dvhdhb7bu", 
  api_key = "644484127367664", 
  api_secret = "w7e3djfOjiJksjkXNxxDJoNpBZc" 
)

@app.get('/home')
def home():
    return 'welcome'

@app.post("/action-recognition/")
async def recognize_action(video_file: UploadFile=File(..., )):
    try:
        print("Torch version:",torch.__version__)
        print("Is CUDA enabled?",torch.cuda.is_available())

        tfile = tempfile.NamedTemporaryFile(delete=False)
        
        tfile.write(await video_file.read())

        print('[INFO] Video successfully read.')

        tmp_dir = tempfile.TemporaryDirectory()
        frame_paths, frames = frame_extract(tfile.name, 480,
                                            tmp_dir.name)
        
        num_frame = len(frame_paths)
        h, w, _ = frames[0].shape
        
        ## download necessary files from the bucket
        # download_files_from_bucket(bucket_files)

        # Get Human detection results.
        det_results, _ = detection_inference(det_config, 
                                            det_checkpoint,
                                            frame_paths, 
                                            DET_SCORE_THR,
                                            device=DEVICE)
        
        print('[INFO] Detection Done.')
  
        # torch.cuda.empty_cache()

        # Get Pose estimation results.
        pose_results, pose_data_samples = pose_inference(pose_config,
                                                        pose_checkpoint,
                                                        frame_paths, det_results,
                                                        DEVICE)
        # torch.cuda.empty_cache()

        fake_anno = dict(
            frame_dir='',
            label=-1,
            img_shape=(h, w),
            original_shape=(h, w),
            start_index=0,
            modality='Pose',
            total_frames=num_frame)
        num_person = max([len(x['keypoints']) for x in pose_results])

        num_keypoint = 17
        keypoint = np.zeros((num_frame, num_person, num_keypoint, 2),
                            dtype=np.float16)
        keypoint_score = np.zeros((num_frame, num_person, num_keypoint),
                                dtype=np.float16)
        for i, poses in enumerate(pose_results):
            keypoint[i] = poses['keypoints']
            keypoint_score[i] = poses['keypoint_scores']

        fake_anno['keypoint'] = keypoint.transpose((1, 0, 2, 3))
        fake_anno['keypoint_score'] = keypoint_score.transpose((1, 0, 2))

        config = mmengine.Config.fromfile(action_config)
        config.merge_from_dict({}) 

        model = init_recognizer(config, checkpoint, DEVICE)
        result = inference_recognizer(model, fake_anno)

        max_pred_index = result.pred_scores.item.argmax().item()
        label_map = [x.strip() for x in open(label_map_txt).readlines()]
        action_label = label_map[max_pred_index]

        response = await visualize(frames, pose_data_samples, action_label)
        response["action_label"] = action_label
        print('[INFO] DONE...')
        tmp_dir.cleanup()
        return response

    except Exception as e:
        return {"error": str(e)}

async def visualize(frames, data_samples, action_label):
    visual_config = mmengine.Config.fromfile(pose_config)
    visualizer = VISUALIZERS.build(visual_config.visualizer)
    visualizer.set_dataset_meta(data_samples[0].dataset_meta)

    vis_frames = []
    print('Drawing skeleton for each frame')
    for d, f in track_iter_progress(list(zip(data_samples, frames))):
        f = mmcv.imconvert(f, 'bgr', 'rgb')
        visualizer.add_datasample(
            'result',
            f,
            data_sample=d,
            draw_gt=False,
            draw_heatmap=False,
            draw_bbox=True,
            show=False,
            wait_time=0,
            out_file=None,
            kpt_thr=0.3)
        vis_frame = visualizer.get_image()
        cv2.putText(vis_frame, action_label, (10, 30), FONTFACE, FONTSCALE,
                    FONTCOLOR, THICKNESS, LINETYPE)
        vis_frames.append(vis_frame)

    vid = mpy.ImageSequenceClip(vis_frames, fps=24)
    vid_path = 'output.mp4'
    vid.write_videofile(vid_path, remove_temp=True)
    return cloudinary.uploader.upload_large(vid_path,  resource_type = "video")

# if __name__ == '__main__':
#     port = int(os.environ.get("PORT", 8080))
#     uvicorn.run(app, host="0.0.0.0", port=port, debug=True)